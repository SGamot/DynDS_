package ds.queue;

import ds.linkedList.LinkedList;

public class Queue<X extends Comparable<X>>
{
  
  //This LinkedList is made from the scratch and is ! the java standard collections lib.
  LinkedList<X> list = new LinkedList<X>();
  
  
  public void enqueue(X data)
  {
    list.add(data);
  }
  
  public X dequeue()
  {
    if(list.getListSize() !=0)
    {
      try
      {
       return list.removeFirst();
      }
      catch (NullPointerException e)
      {
        e.getMessage();
        System.out.println("No elements left in the Queue");
      }
    }
    return null;
  }
  
  public boolean contains(X data)
  {
    return list.contains(data);
  }
  
  public void printQueue()
  {
    list.printList();
  }
  
  
}
