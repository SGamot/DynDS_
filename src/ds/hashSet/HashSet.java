package ds.HashSet;

import ds.linkedList.LinkedList;

public class HashSet<X>
{
  LinkedList set = new LinkedList();
  
  private class Entry<X>
  {
    X element;
  
    public X getElement()
    {
      return element;
    }
  
    public void setElement(X element)
    {
      this.element = element;
    }
  }
}
