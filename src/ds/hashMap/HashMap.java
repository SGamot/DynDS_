package ds.HashMap;

import ds.linkedList.LinkedList;

public class HashMap<X,Y>
{
  LinkedList hashList = new LinkedList();
  private class HashEntry<X,Y>
  {
    private X key;
    private Y value;
  
    public X getKey()
    {
      return key;
    }
  
    public void setKey(X key)
    {
      this.key = key;
    }
  
    public Y getValue()
    {
      return value;
    }
  
    public void setValue(Y value)
    {
      this.value = value;
    }
  }
  
}
