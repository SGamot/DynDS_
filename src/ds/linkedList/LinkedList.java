package ds.linkedList;

public class LinkedList<X extends Comparable<X>>
{
  
  private Node first;
  private Node last;
  private int size;
  
  
  
  private  class Node
  {
    
    X data;
    Node next;
    Node prev;
    
    
    public Node(X data)
    {
      this.data = data;
    }
  
    public X getData()
    {
      return data;
    }
  
    public void setData(X data)
    {
      this.data = data;
    }
  
    public Node getNext()
    {
      return next;
    }
  
    public void setNext(Node next)
    {
      this.next = next;
    }
  
    public Node getPrev()
    {
      return prev;
    }
  
    public void setPrev(Node prev)
    {
      this.prev = prev;
    }
  }
  
  public void add(X data)
  {
    Node node = new Node(data);
    
    if(first != null)
    {
      node.setPrev(last);
      last.setNext(node);
      last = node;
    }
    else
    {
      first = last = node;
    }
    
    size++;
  }
  
  
  public boolean contains(X data)
  {
    return getNode(data)!=null;
  }
  
  
  private Node getNode(X data)
  {
    Node node = first;
    if(first != null)
    {
      do
      {
        if(node.getData().compareTo(data) ==0)
        {
          return node;
        }
      }while((node = node.getNext() )!= null);
    }
    return null;
  }
  
  private Node getPrevNode(Node node)
  {
      return node.getPrev();
  }
  
  
  public void insertAt(X data , int index)
  {
    if(index<=0 || index>size)
    {
      System.out.println("Enter index within the List.");
      throw new IndexOutOfBoundsException();
    }
    else
    {
  
      if (index == 1)
        insertAtStart(data);
      else if (index == size)
        insertAtEnd(data);
      else
      {
        Node insertNode = new Node(data);
        Node node = null;
        Node prev = null;
        try
        {
          node = getNodeAtIndex(index);
      
          if (node == null)
            throw new NullPointerException();
        } catch (NullPointerException e)
        {
        }
        prev = node.getPrev();
        insertNode.setNext(node);
        prev.setNext(insertNode);
        node.setPrev(insertNode);
        insertNode.setPrev(prev);
        size++;
      }
    }
  }
  
  
  
  public void insertAtStart(X data)
  {
    Node node = new Node(data);
    node.setNext(first);
    first = node;
    size++;
  }
  
  public void insertAtEnd(X data)
  {
    Node node = new Node(data);
    last.setNext(node);
    node.setPrev(last);
    last = node;
    size++;
  }
  
  public Node getNodeAtIndex(int index)
  {
    Node node = first;
    
    if(first != null)
    {
      do
      {
        index--;
      }while (index!=0 && (node = node.getNext())!=null);
      return node;
    }
    return null;
    
  }
  
  public int getListSize()
  {
    return size;
  }
  
  
  public void remove(X data)
  {
    Node node = getNode(data);
    Node prev;
    Node next;

    
    if(node != null && first != last)// checking if a node with the data actually exist
    {
  
      try
      {
        next = node.getNext();
        if(next == null)
          throw new NullPointerException();
      }
      catch (NullPointerException e) //if null then it is the last node of the list
      {
        size--;
        node.getPrev().setNext(null); // so removing the last node by making the prev node to point to null
        return;
      }
      try
      {
        prev = node.getPrev();
        if(prev == null)
          throw new NullPointerException();
      }
      catch(NullPointerException e) //if null then it is the first node of the list.
      {
        size--;
        node.setPrev(null);
        first = node.getNext(); //moving the first pointer of the list to the next immediate node. Hence as no reference pointing to the earlier node, it is removed.
        return;
      }
      
      prev.setNext(next);
      next.setPrev(prev);
      size--;
    }
    else if(first == last)
    {
      size--;
      first = last = null;
    }
    else
      System.out.println( data + " not Found in List");
    }
    
    public X removeFirst()
    {
      X data = null;
      Node node = null;
      if(first != null)
         data = first.getData();
      
      try
      {
        node = first.getNext();
        if(node != null)
          first = node;
        else
          first = last = null;
        size--;
      }
      catch (NullPointerException e)
      {
        e.getMessage();
        System.out.println("List is Empty");
      }
      
      return data;
    }
    
    public X removeLast()
    {
      X data;
      
      if(last != null)
      {
        data = last.getData();
  
        if(first != last)
        {
          last = getPrevNode(last);
          last.setNext(null);
        }
        else
        {
          first = last = null;
        }
      }
      else
      {
        throw new NullPointerException();
      }
      size--;
      return data;
    }
    
    public  void removeAll(X data)
    {
      while(contains(data))
      {
        remove(data);
        size--;
      }
    }
    
    
    public void printList()
    {
      Node node = first;
      
      if(first != null)
      {
        do
        {
          System.out.print(node.getData() + " ");
        }
        while ((node = node.getNext()) != null);
//        System.out.print("null");
        System.out.println("\n");
      }
      else
        System.out.println("List is Empty");
    }
    
    
    public void deleteList()
    {
      last = null;
      first = null;
    }
    
    
  }

  

