package ds.stack;

public class Stack<X extends Comparable<X>>
{
  Node top;
  int size;
  
  private class Node
  {
    private X data;
    private Node prev;
    
    private Node()
    {
    
    }
    
    Node(X data)
    {
      this.data = data;
    }
  
    public void setPrev(Node prev)
    {
      this.prev = prev;
    }
  
    public Node getPrev()
    {
      return prev;
    }
    
    public X getData()
    {
      return data;
    }
    
    public void setData(X data)
    {
      this.data = data;
    }
  }
  
  public int getSize()
  {
    return size;
  }
  
  public void push(X data)
  {
    Node node = new Node(data);
    size++;
    
    if(top != null)
    {
      node.setPrev(top);
      top = node;
    }
    else
    {
      top = node;
    }
    
  }
  
  
  
  public X peek() throws EmptyStackException
  {
    if(top!=null)
      return top.getData();
    else
      throw new EmptyStackException();
  }
  
  
  public X pop() throws EmptyStackException
  {
   
    if(top == null)
    {
      throw new EmptyStackException();
    }
    if(top.getPrev() == null)
    {
      size--;
      X data = top.getData();
      top = null;
      return data;
    }
    else
    {
      size--;
      X data = top.getData();
      top = top.getPrev();
      return data;
    }
  }
  
  
  public boolean contains(X data) throws EmptyStackException
  {
    Node iterator = top;
    
    if(top != null)
    {
  
      do
      {
        if(iterator.getData().compareTo(data)==0)
          return true;
      }
      while ((iterator = iterator.getPrev()) != null);
    }
    else
      throw new EmptyStackException();
    
    return false;
  }
  
  public void printStack() throws EmptyStackException
  {
    Node iterator = top;
  
    if(top != null)
    {
    
      do
      {
        System.out.println(iterator.getData());
        System.out.println("↑");
      }
      while ((iterator = iterator.getPrev()) != null);
      System.out.println("null");
      System.out.println("\n");
      System.out.println("\n");
    }
    else
      throw new EmptyStackException();
  }
  
}
