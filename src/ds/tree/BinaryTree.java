package ds.tree;

public class BinaryTree<X extends Comparable<X>>
{
  private Node root;
  private int size;
  
  public Node getRoot()
  {
    return root;
  }
  
  public BinaryTree()
  {
    root = null;
  }
  
  public BinaryTree(X data)
  {
    root = new Node(data);
  }
  
  
  public void add(X data)
  {
    Node node = new Node(data);
    
    if(root == null)
    {
      root = node;
      size++;
    }
    else
    {
      insert(root , node);
    }
  }
  
  
  
  private void insert(Node parent , Node child)
  {
      if(child.getData().compareTo(parent.data)<0)
      {
        if(parent.getLeftChild() == null)
        {
          parent.setLeftChild(child);
          child.setParent(parent);
          this.size++;
        }
        else
        {
          insert(parent.getLeftChild(),child);
        }
      }
      
      else if(child.getData().compareTo(parent.data)>0)
      {
        if(parent.getRightChild() == null)
        {
          parent.setRightChild(child);
          child.setParent(parent);
          this.size++;
        }
        else
          insert(parent.getRightChild(),child);
        
      }
  }

  
  public boolean contains(X data)
  {
    
    return findNode(data , root) != null;
  }
  
  
  private Node findNode(X data , Node node)
  {
    int val  = node.getData().compareTo(data);
    
    if(val ==0)
      return node;
    
    
    if(val>0)
    {
      try
      {
        return findNode(data , node.getLeftChild());
      }
      catch(NullPointerException e)
      {
        return  null;
      }
    }
    else if(val<0)
    {
      try
      {
        return findNode(data , node.getRightChild());
      }
      catch(NullPointerException e)
      {
        return null;
      }
    }

    return null;
  }
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  public void remove(X data)
  {
    
  
      try
      {
        unlink(findNode(data , root));
      }
      catch (Exception e)
      {
        System.out.println("null argument in unlink in remove");
      }
    
  }
  
  
  private void unlink(Node node)
  {
    Node parent = node.getParent();
    Node left = node.getLeftChild();
    Node right = node.getRightChild();
    
    boolean isLeftPresent = left != null;
    boolean isRightPresent = right != null;
    
    node = null;
    if(parent != null)
    {
      if(isLeftPresent && isRightPresent)
      {
         insert(parent,right);
         insert(parent,left);
      }

      else if(isRightPresent)
        insert(parent,right);
      else if(isLeftPresent)
        insert(parent,left);
      
    }
    else
    {
      if(isLeftPresent && isRightPresent)
      {
        root = right;
        left.setParent(right);
      }
  
      else if(isRightPresent)
        root = right;
      else if(isLeftPresent)
        root = left;
      else
        root = null;
    }
    
    
  }
  
  
  public void printInorder(Node node)
  {
    if (node == null)
      return;
    
    /* first recur on left child */
    printInorder(node.getLeftChild());
    
    /* then print the data of node */
    System.out.print(node.getData() + " ");
    
    /* now recur on right child */
    printInorder(node.getRightChild());
    
  }
  
  public void printPreorder(Node node)
  {
    if (node == null)
      return;
    
    /* first print data of node */
    System.out.print(node.getData()+ " ");
    
    /* then recur on left sutree */
    printPreorder(node.getLeftChild());
    
    /* now recur on right subtree */
    printPreorder(node.getRightChild());
  }
  
  public void printPostorder(Node node)
  {
    if (node == null)
      return;
    
    // first recur on left subtree
    printPostorder(node.getLeftChild());
    
    // then recur on right subtree
    printPostorder(node.getRightChild());
    
    // now deal with the node
    System.out.print(node.getData() + " ");
  }
  
  
  
  
  
  
  
  
  
  
  
  
  private class Node
  {
    X data;
    Node leftChild;
    Node rightChild;
    Node parent;
    
    Node(X data)
    {
      this.data = data;
      leftChild = rightChild = parent = null;
    }
  
    public X getData()
    {
      return data;
    }
  
    public void setData(X data)
    {
      this.data = data;
    }
  
    public Node getLeftChild()
    {
      return leftChild;
    }
  
    public void setLeftChild(Node leftChild)
    {
      this.leftChild = leftChild;
    }
  
    public Node getRightChild()
    {
      return rightChild;
    }
  
    public void setRightChild(Node rightChild)
    {
      this.rightChild = rightChild;
    }
  
    public Node getParent()
    {
      return parent;
    }
  
    public void setParent(Node parent)
    {
      this.parent = parent;
    }
  }

}
