package ds.automata;

public class Automata_test
{
  public static void main(String[] args)
  {
    State s0 = new State();
    State s1 = new State();
    State s2 = new State();
    State s3 = new State();
    
    Automata<Character> am = new Automata<>();
    
    s0.addTransition('1',s0).addTransition('0',s1);
    s1.addTransition('0',s1).addTransition('1',s2);
    
    s2.addTransition('1',s3).addTransition('0',s1);
    s3.addTransition('0',s1).addTransition('1',s0);
    
    
    am.addState("s0",s0);
    am.addState("s1",s1);
    am.addState("s2",s2);
    am.addState("s3",s3);
    
    am.setInitialState(s0);
    am.setFinalState(s3);
    
    //accept string ending in 011
    
    String s = "00101011";
    System.out.println(am.isStringAcceptable(s));
    
  }
}
