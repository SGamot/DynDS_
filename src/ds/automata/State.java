package ds.automata;

import java.util.HashMap;
import java.util.Map;

class State<X>
{
  private Map transitions = new HashMap<X,State<X>>();
  
  
  public State addTransition(X input , State nextState)
  {
    transitions.put(input, nextState);
    return this;
  }
  
  public State addMultipleTranstitions( Map<X,State<X>> transitionMap)
  {
    transitions.putAll(transitionMap);
    return this;
  }
  
  public boolean hasTransition(X input)
  {
    return transitions.containsKey(input);
  }
  
  public State removeTransition(X input)
  {
    transitions.remove(input);
    return this;
  }
  
  public State clearTransitions()
  {
    transitions.clear();
    return this;
  }
  
  public State getNextStateOnTransition(X input)
  {
    return (State)transitions.get(input);
  }
  public boolean isStateReachableFromCurrentState(State s)
  {
    return transitions.containsValue(s);
  }
  
  public boolean isStateEmpty()
  {
    return transitions.isEmpty();
  }
  
}
