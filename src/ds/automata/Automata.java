package ds.automata;

import java.util.HashMap;
import java.util.Map;

class Automata<X>
{
  
  private Map states = new HashMap<String,State<X>>();
  private State currentState, initialState, finalState;
  
  public Automata()
  {
  }
  public Automata(String name,State s)
  {
    currentState = initialState = finalState = s;
    states.put(name,s);
  }

  public State getStateFromKey(String key)
  {
    return (State)states.get(key);
  }
  
  public void setInitialState(State s)
  {
    initialState = currentState = s;
  }
  public void setFinalState(State s)
  {
    finalState = s;
  }
  
  public State getInitialState()
  {
    return initialState;
  }
  public State getFinalState()
  {
    return finalState;
  }
  
  public State getCurrentState()
  {
    return currentState;
  }
  public boolean statePresent(State s)
  {
    return states.containsValue(s);
  }
  
  public Automata addState(String stateName)
  {
    states.put(stateName, new State());
    return this;
  }
  public Automata addState(String stateName , State s)
  {
    states.put(stateName , s);
    return this;
  }
  
  public Automata addMultipleStates(Map<String,State<X>> stateMap)
  {
    states.putAll(stateMap);
    return this;
  }
  
  public void transit(char in)
  {
    if(currentState.hasTransition(in))
      currentState = currentState.getNextStateOnTransition(in);
    else
      throw new IllegalArgumentException("CurrentState " + currentState +" doesn't have transition : " + in);
  }
  
  public boolean isStringAcceptable(String s)
  {
    if(currentState == null)
      return false;
    if(currentState.isStateEmpty())
      return false;
    
    char[] arr = s.toCharArray();
    for(char c : arr)
    {
      try
      {
        transit(c);
      }
      catch(IllegalArgumentException e)
      {
        System.err.println(e.getMessage());
        return false;
      }
    }
    
    return currentState == finalState;
  }
  
  
  
}
