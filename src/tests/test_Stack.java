package tests;

import ds.stack.EmptyStackException;
import ds.stack.Stack;


import java.util.Scanner;

public class test_Stack
{
  
  public static void main(String[] args)
  {
    Scanner sc = new Scanner(System.in);
    String s = sc.next();
    while (!"end".equals(s))
    {
      try
      {
        System.out.println(performMagic(s));
      } catch (EmptyStackException e)
      {
        System.out.println("LUL");
      }
      
      s = sc.next();
    }
    
  }
  
  private static boolean performMagic(String s) throws EmptyStackException
  {
    if (s.length() == 0)
    {
      return true;
    }
    
    Stack<Character> stack = new Stack<>();
    
    for (int i = 0; i < s.length(); i++)
    {
      char c = s.charAt(i);
      switch (c)
      {
      case '{':
      case '[':
      case '(':
        stack.push(c);
        break;
      
      case ')':
                if (stack.pop() != '(')
                  return false;
                break;
      
      case ']':
                if (stack.pop() != '[')
                  return false;
                break;
      case '}':
                if (stack.pop() != '{')
                  return false;
                break;
      default:
                System.out.println("Non parantheses char detected ");
      }
    }
    if(stack.getSize() ==0)
      return true;
    else
      return false;
  }
  
}
