package tests;
import ds.tree.*;

import java.util.Random;

public class test_Tree
{
  public static void main(String[] args)
  {
    BinaryTree<Integer> tree = new BinaryTree<>();
    int val=0;
    boolean done =false;
    Random r = new Random();
    for(int i = 0 ; i < 15; i++)
    {
      int v = r.nextInt(100);
      if(i == r.nextInt(10) && !done)
      {
        val = v;
        done = true;
        System.out.println("selected " + val);
      }
      tree.add(v);
    }
  
    
    tree.printInorder(tree.getRoot());
    System.out.println(tree.getRoot());
    System.out.println("\n");
  
  
    
    tree.printInorder(tree.getRoot());
    System.out.println(tree.getRoot());
    System.out.println("\n");
    
    tree.printPreorder(tree.getRoot());
    System.out.println(tree.getRoot());
    System.out.println("\n");
  
    tree.printPostorder(tree.getRoot());
    System.out.println(tree.getRoot());
    System.out.println("\n");
  
  }
  
}
