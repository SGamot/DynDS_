package tests;

import ds.linkedList.LinkedList;

public class test_List
{
  public static void main(String[] args)
  {
    LinkedList list  = new LinkedList();
    for(int i = 0; i<20; i++)
    {
      if(i%2==0)
        list.add("Even");
      else
        list.add("Odd");
    }
    
    list.add("lol");
    list.add(15);
    list.printList();
    System.out.println(list.getListSize());
    list.removeLast();
    list.printList();
    System.out.println(list.getListSize());
    list.removeFirst();
    System.out.println(list.getListSize());
    list.printList();
    list.remove("lol");
    System.out.println(list.getListSize());
    list.printList();
  }
}
