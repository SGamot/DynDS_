package tests;

import ds.queue.Queue;

public class test_Queue
{
  public static void main(String[] args)
  {
    Queue<Integer> queue = new Queue<>();
    for (int i = 0; i < 5; i++)
    {
      queue.enqueue(i);
      queue.printQueue();
    }

    queue.dequeue();
    queue.printQueue();
    queue.dequeue();
    queue.printQueue();
    
  }
}
